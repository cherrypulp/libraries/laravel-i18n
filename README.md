# I18N Blok Package

[![Packagist](https://img.shields.io/packagist/v/blok/i18n.svg)](https://packagist.org/packages/blok/i18n)
[![Packagist](https://poser.pugx.org/blok/i18n/d/total.svg)](https://packagist.org/packages/blok/i18n)
[![Packagist](https://img.shields.io/packagist/l/blok/i18n.svg)](https://packagist.org/packages/blok/i18n)

Package description: 

Simple Laravel helper to export all the languages files in a javascript file.

## Installation

Install via composer
```bash
composer require blok/i18n
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Blok\I18n\I18nServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
'I18n' => Blok\I18n\Facades\I18n::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Blok\I18n\I18nServiceProvider" --tag="config"
```

## Usage

Just call :

`<script src="/js/lang.js?v={$cache versionning}"></script>`

And you will have a window.i18n variable available with all the localization.

## Security

If you discover any security related issues, please email 
instead of using the issue tracker.

## Credits

- [](https://github.com/blok/i18n)
- [All contributors](https://github.com/blok/i18n/graphs/contributors)

This package is bootstrapped with the help of
[blok/laravel-package-generator](https://github.com/blok/laravel-package-generator).
