<?php

namespace Blok\I18n\Tests;

use Blok\I18n\Facades\I18n;
use Blok\I18n\ServiceProvider;
use Orchestra\Testbench\TestCase;

class I18nTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'i18n' => I18n::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
