<?php

return [
    "updated" => ":attribute a bien été mis à jour",
    "created" => ":attribute a bien été créé",
    "deleted" => ":attribute a bien été supprimé",
];
