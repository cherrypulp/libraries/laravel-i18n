<?php

namespace Blok\I18n;

class I18nServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/i18n.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('i18n.php'),
        ], 'config');

        $this->loadRoutesFrom(__DIR__.'/routes.php');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'i18n'
        );

        $this->app->bind('i18n', function () {
            return new I18n();
        });
    }
}
