<?php

Route::get(config('i18n.filename'), function () {

    if (request()->has('lang')) {
        $lang = request()->get('lang');
    } else {
        $lang = app()->getLocale();
    }

    $ref = request()->path().request()->get('v').$lang;

    if (request()->has("cache_clear")) {
        Cache::forget($ref);
    }

    $strings = Cache::remember($ref, request()->has("cache_clear") ? 0 : config("i18n.cache_duration", 0), function () use ($lang) {

        $files   = glob(resource_path('lang/' . $lang . '/*.php'));
        $json = resource_path('lang/' . $lang . '.json');
        $strings = [];

        //if database laravel mode is enabled
        if (config('translation.driver') === 'database' && class_exists('\Blok\Translation\Providers\Translator')) {
            $translations = \Blok\Translation\Providers\Translator::loadTranslationsFromDatabase();
            $strings = $translations[$lang];
        }

        if (file_exists($json)) {
            $json = file_get_contents($json);
            $strings = array_merge($strings, json_decode($json, true));
            $strings = \Blok\Utils\Arr::dot_array($strings);
        }

        foreach ($files as $file) {
            $name           = basename($file, '.php');
            $strings[$name] = require $file;
        }

        $files   = glob(resource_path('lang/' . $lang . '/**/*.php'));

        foreach ($files as $file) {
            $path = str_replace(base_path('resources/lang/'), '', $file);
            $part = explode('/', $path);
            unset($part[0]);
            array_pop($part);
            $name           = basename($file, '.php');
            $strings[implode('/', $part).'/'.$name] = require $file;
        }

        return $strings;
    });

    header('Content-Type: text/javascript');
    header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0');
    echo('window.'.config('i18n.global', 'translations').' = ' . json_encode($strings) . ';');
    exit();

})->name('assets.lang');
